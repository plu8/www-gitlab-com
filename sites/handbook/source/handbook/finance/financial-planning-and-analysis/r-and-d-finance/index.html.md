---
layout: handbook-page-toc
title: "R&D Finance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the R&D Finance Handbook!

## Finance Business Partner Alignment
Shuang Shackelford, Director, FP&A @sshackleford

Ellen Boyd, Senior Manager, FP&A @eboyd1

| Department | Name | Function |
| -------- | ---- | -------- |
| Development | @JessSmith | Sr. FP&A |
| UX | @JessSmith | Sr. FP&A |
| Security | @JessSmith | Sr. FP&A |
| Quality | @JessSmith | Sr. FP&A |
| Infrastructure | @JessSmith | Sr. FP&A |
| Hosting | @sshackleford | FP&A Manager |
| Incubation Engineering | @JessSmith | Sr. FP&A |
| Product Management | @JessSmith | Sr. FP&A |
| Customer Support | @JessSmith | Sr. FP&A |


Shuang and Ellen can approve Coupa and Greenhouse Reqs as needed for Finance Business Partners.  
## Common Links

- [Financial Planning & Analysis (FP&A)](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/)
- [Finance Issue Tracker](https://gitlab.com/gitlab-com/finance/issues)
